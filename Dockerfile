FROM arm64v8/openjdk:20-slim
ADD ./build/libs/*-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
